from django.urls import path
from receipts.views import show_receipt_list

urlpatterns = [
    path("", show_receipt_list, name="home"),
]
