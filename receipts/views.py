from django.shortcuts import render
from receipts.models import Receipt

# Create your views here.


def show_receipt_list(request):
    receipt_list = Receipt.objects.all()
    context = {"receipt_list": receipt_list}
    return render(request, "receipts_list/list.html", context)
